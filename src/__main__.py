import sys
import csv

from Normalizers.TimestampNormalizer import TimestampNormalizer
from Normalizers.DurationNormalizer import DurationNormalizer
from Normalizers.NameNormalizer import NameNormalizer
from Normalizers.ZipCodeNormalizer import ZipCodeNormalizer
from UnicodeParser import UnicodeParser


def main():
    headers = ['Timestamp', 'Address', 'ZIP', 'FullName', 'FooDuration', 'BarDuration', 'TotalDuration', 'Notes']
    reader = csv.DictReader(sys.stdin, fieldnames=headers, delimiter=",", quotechar='"')
    writer = csv.DictWriter(sys.stdout, fieldnames=headers)
    next(reader)

    errors = []
    sys.stdout.write("\n")
    writer.writeheader()
    for row in reader:
        try:
            timestamp_normalized = TimestampNormalizer(row[headers[0]]).normalize()
            zipcode_normalized = ZipCodeNormalizer(row[headers[2]]).normalize()
            name_normalized = NameNormalizer(row[headers[3]]).normalize()
            foo_duration_normalized = DurationNormalizer(row[headers[4]]).normalize()
            bar_duration_normalized = DurationNormalizer(row[headers[5]]).normalize()

            writer.writerow({
                                headers[0]: timestamp_normalized,
                                headers[1]: UnicodeParser.parse(row[headers[1]]),
                                headers[2]: zipcode_normalized,
                                headers[3]: name_normalized,
                                headers[4]: foo_duration_normalized,
                                headers[5]: bar_duration_normalized,
                                headers[6]: str(float(foo_duration_normalized) + float(bar_duration_normalized)),
                                headers[7]: UnicodeParser.parse(row[headers[7]])
                             })
            sys.stdin.flush()
        except Exception as e:
            errors.append(str(e))

if __name__ == "__main__":
    main()