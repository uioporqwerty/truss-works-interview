from datetime import datetime

import pytz

from Normalizers.Normalizer import Normalizer


class TimestampNormalizer(Normalizer):
    __DATETIME_FORMAT = '%m/%d/%y %I:%M:%S %p'

    def normalize(self) -> str:
        normalized = ""

        if self.raw_input:
            try:
                parsed_datetime = datetime.strptime(self.raw_input, self.__DATETIME_FORMAT)
                pst = pytz.timezone('US/Pacific')
                est = pytz.timezone('US/Eastern')
                parsed_datetime = pst.localize(parsed_datetime)

                est_datetime = parsed_datetime.astimezone(tz=est)
                hour = est_datetime.hour if est_datetime.hour <= 12 else est_datetime.hour - 12

                normalized = '{dt.month}/{dt.day}/{dt:%y} {hour}:{dt:%M}:{dt:%S} {dt:%p}'.format(dt=est_datetime, hour=hour)
            except:
                raise ValueError("Unable to parse date time.")

        return normalized