import time
from Normalizers.Normalizer import Normalizer


class DurationNormalizer(Normalizer):
    def normalize(self) -> str:
        if not self.raw_input:
            return ""
        if self.__isValidDurationFormat():
            return self.__getSeconds()
        else:
            raise ValueError("Invalid duration format. Expected HH:MM:SS.MS")

    def __isValidDurationFormat(self) -> bool:
        try:
            time.strptime(self.raw_input, '%H:%M:%S.%f')
            return True
        except ValueError:
            return False

    def __getSeconds(self) -> str:
        hours, minutes, seconds = self.raw_input.split(":")
        seconds, milliseconds = seconds.split(".")

        return str(float(int(hours) * 3600 + int(minutes) * 60 + int(seconds) + (int(milliseconds) / 1000)))