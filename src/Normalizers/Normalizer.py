from abc import ABC, abstractmethod


class Normalizer(ABC):
    def __init__(self, raw_input: str):
        self.raw_input = raw_input

    @abstractmethod
    def normalize(self) -> str:
        pass