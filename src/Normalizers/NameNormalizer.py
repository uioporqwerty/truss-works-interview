from Normalizers.Normalizer import Normalizer
from UnicodeParser import UnicodeParser


class NameNormalizer(Normalizer):
    def normalize(self) -> str:
        return UnicodeParser.parse(self.raw_input.upper()) if self.raw_input else ""