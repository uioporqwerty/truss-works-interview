from Normalizers.Normalizer import Normalizer


class ZipCodeNormalizer(Normalizer):
    __MAX_LENGTH = 5

    def normalize(self) -> str:
        if not self.raw_input:
            return "".zfill(self.__MAX_LENGTH)

        if len(self.raw_input) > 0 and not self.raw_input.isdigit():
            raise ValueError("Zip code does not contain all digits.")

        return self.raw_input.zfill(self.__MAX_LENGTH)