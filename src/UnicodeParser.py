from ftfy import fix_encoding


class UnicodeParser:
    @staticmethod
    def parse(raw_input):
        parsed = ""

        if raw_input:
            parsed = fix_encoding(raw_input)

        return parsed