from unittest import TestCase

from Normalizers.DurationNormalizer import DurationNormalizer


class TestDurationNormalizer(TestCase):
    def setUp(self):
        self.normalizer = DurationNormalizer("")

    def test_normalize_WithValidFormat_ReturnsSeconds(self):
        expected = "5553.123"
        self.normalizer.raw_input = "1:32:33.123"

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithEmptyDuration_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = ""

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithNullDuration_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = None

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithInvalidFormat_RaisesError(self):
        self.normalizer.raw_input = "abcdef"
        self.assertRaises(ValueError, self.normalizer.normalize)