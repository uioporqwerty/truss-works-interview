from unittest import TestCase

from Normalizers.ZipCodeNormalizer import ZipCodeNormalizer


class TestZipCodeNormalizer(TestCase):
    def setUp(self):
        self.normalizer = ZipCodeNormalizer("")

    def test_normalize_WithSingleDigit_ReturnsPaddedZipCode(self):
        expected = "00001"
        self.normalizer.raw_input = "1"

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithFullZipCode_ReturnsFullZipCode(self):
        expected = "12345"
        self.normalizer.raw_input = "12345"

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithAlphaZipCode_ThrowsException(self):
        self.normalizer.raw_input = "12345a"
        self.assertRaises(ValueError, self.normalizer.normalize)

    def test_normalize_WithEmptyString_ReturnsAllZerosZipCode(self):
        expected = "00000"
        self.normalizer.raw_input = ""

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithNullString_ReturnsAllZerosZipCode(self):
        expected = "00000"
        self.normalizer.raw_input = None

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)