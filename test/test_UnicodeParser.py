from unittest import TestCase

from UnicodeParser import UnicodeParser


class TestUnicodeParser(TestCase):
    def setUp(self):
        self.invalidtext = [
            {
                "Expected": "🏳️🏴🏳️🏴",
                "Input": "üè≥Ô∏èüè¥üè≥Ô∏èüè¥"
            },
            {
                "Expected": "I like Emoji! 🍏🍎😍",
                "Input": "I like Emoji! üçèüçéüòç"
            }
        ]

    def test_parse_WithInvalidUtf8_ReturnsValid(self):
        for text in self.invalidtext:
            expected = text.get("Expected")
            raw_input = text.get("Input")

            actual = UnicodeParser.parse(raw_input)

            self.assertEqual(expected, actual)

    def test_parse_WithEmptyUtf8_ReturnsEmpty(self):
        expected = ""

        actual = UnicodeParser.parse("")

        self.assertEqual(expected, actual)

    def test_parse_WithNullString_ReturnsEmpty(self):
        expected = ""

        actual = UnicodeParser.parse(None)

        self.assertEqual(expected, actual)