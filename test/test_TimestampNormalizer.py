from unittest import TestCase

from Normalizers.TimestampNormalizer import TimestampNormalizer


class TestTimestampNormalizer(TestCase):
    def setUp(self):
        self.normalizer = TimestampNormalizer("")
        self.dates = [{
                "Expected": "10/6/12 1:31:11 AM",
                "Input": "10/5/12 10:31:11 PM"
            },
            {
                "Expected": "5/12/10 7:48:12 PM",
                "Input": "5/12/10 4:48:12 PM"
            }]

    def test_normalize_WithPSTDates_ReturnsESTDates(self):
        for date in self.dates:
            expected = date.get("Expected")
            raw_input = date.get("Input")

            self.normalizer.raw_input = raw_input

            actual = self.normalizer.normalize()

            self.assertEqual(expected, actual)

    def test_normalize_WithInvalidDate_ThrowsException(self):
        self.normalizer.raw_input = "hello world"
        self.assertRaises(ValueError, self.normalizer.normalize)

    def test_normalize_WithEmptyDate_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = ""

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithNullDate_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = None

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)