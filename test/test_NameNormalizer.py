from unittest import TestCase

from Normalizers.NameNormalizer import NameNormalizer


class TestNameNormalizer(TestCase):
    def setUp(self):
        self.normalizer = NameNormalizer("")

    def test_normalize_WithValidName_ReturnsUppercase(self):
        expected = "NITISH SACHAR"
        self.normalizer.raw_input = "nitish sachar"

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithEmptyName_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = ""

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)

    def test_normalize_WithNullName_ReturnsEmpty(self):
        expected = ""
        self.normalizer.raw_input = None

        actual = self.normalizer.normalize()

        self.assertEqual(expected, actual)
