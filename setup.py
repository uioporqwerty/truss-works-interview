from setuptools import setup

setup(
        name='truss-works-interview',
        version='1.0.0',
        install_requires = ['pytz', 'ftfy'],
        packages=[],
        package_dir={ '': 'src' },
        url='',
        license='',
        author='Nitish Sachar',
        author_email='nitish.sachar@protonmail.com',
        description=''
)
